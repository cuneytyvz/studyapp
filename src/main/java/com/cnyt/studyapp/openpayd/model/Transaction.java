package com.cnyt.studyapp.openpayd.model;

import com.cnyt.studyapp.openpayd.dto.TransactionDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name="transaction")
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(precision = 10, scale = 4)
    private BigDecimal sourceAmount;
    private String sourceCurrency;
    @Column(precision = 10, scale = 4)
    private BigDecimal targetAmount;
    private String targetCurrency;

    public Transaction() {
    }

    public Transaction(Long id, Date date, BigDecimal sourceAmount, String sourceCurrency, BigDecimal targetAmount, String targetCurrency) {
        this.id = id;
        this.date = date;
        this.sourceAmount = sourceAmount;
        this.sourceCurrency = sourceCurrency;
        this.targetAmount = targetAmount;
        this.targetCurrency = targetCurrency;
    }

    public Transaction(Date date, BigDecimal sourceAmount, String sourceCurrency, BigDecimal targetAmount, String targetCurrency) {
        this.date = date;
        this.sourceAmount = sourceAmount;
        this.sourceCurrency = sourceCurrency;
        this.targetAmount = targetAmount;
        this.targetCurrency = targetCurrency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(BigDecimal sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public TransactionDto toDto() {
        return new TransactionDto(id,new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date),sourceAmount,sourceCurrency,targetAmount,targetCurrency);
    }
}
