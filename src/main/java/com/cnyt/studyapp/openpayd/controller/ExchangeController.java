package com.cnyt.studyapp.openpayd.controller;

import com.cnyt.studyapp.openpayd.dto.ResponseObject;
import com.cnyt.studyapp.openpayd.service.ExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@RestController
public class ExchangeController {
    Logger logger = LoggerFactory.getLogger(ExchangeController.class);

    @Autowired
    private ExchangeService exchangeService;

    @GetMapping("/exchangeRate")
    public ResponseEntity<ResponseObject> exchangeRate(@RequestParam(value="source") String source,@RequestParam(value="target") String target) {
        return new ResponseEntity<ResponseObject>(new ResponseObject("Success",HttpStatus.OK.value(),exchangeService.getExchangeRate(source,target)), HttpStatus.OK);
    }

    @GetMapping("/conversion")
    public ResponseEntity<ResponseObject> conversion(@RequestParam(value="amount")BigDecimal amount,@RequestParam(value="source") String source,@RequestParam(value="target") String target) {
        return new ResponseEntity<ResponseObject>(new ResponseObject("Success",HttpStatus.CREATED.value(),exchangeService.getConversion(amount,source,target)), HttpStatus.CREATED);
    }

    @GetMapping("/conversions")
    public ResponseEntity<ResponseObject> conversions(@RequestParam(value="id", required = false)Long id,
                                                      @RequestParam(value="date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                                      @RequestParam(value="page", required = false) Integer page,
                                                      @RequestParam(value="pageSize", required = false) Integer pageSize) {
        if(Objects.isNull(id) && Objects.isNull(date)) {
            logger.error("/conversions : Id or date is null.");
            return new ResponseEntity<ResponseObject>(new ResponseObject("Id or date must be given", HttpStatus.BAD_REQUEST.value(),null),HttpStatus.BAD_REQUEST);
        }
        if(Objects.isNull(id) && (Objects.isNull(page) || Objects.isNull(pageSize))) {
            logger.error("/conversions : Page of page size is null.");
            return new ResponseEntity<ResponseObject>(new ResponseObject("Page and page size must be given", HttpStatus.BAD_REQUEST.value(),null),HttpStatus.BAD_REQUEST);
        }
        if(Objects.isNull(id) && (page < 1 || pageSize < 1)) {
            logger.error("/conversions : Page or page size are less than 1.");
            return new ResponseEntity<ResponseObject>(new ResponseObject("Page and page size must be bigger than 0", HttpStatus.BAD_REQUEST.value(),null),HttpStatus.BAD_REQUEST);
        }
        if(Objects.isNull(id)) {
            page = page - 1;
        }
        return new ResponseEntity<ResponseObject>(new ResponseObject("Success",HttpStatus.OK.value(),exchangeService.getConversionList(id,date,page,pageSize)), HttpStatus.OK);
    }
}
