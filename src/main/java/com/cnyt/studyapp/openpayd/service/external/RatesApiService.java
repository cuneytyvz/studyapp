package com.cnyt.studyapp.openpayd.service.external;

import org.springframework.stereotype.Component;

@Component
public interface RatesApiService {
    Double getExchangeRate(String sourceCurrency, String targetCurrency);
}
