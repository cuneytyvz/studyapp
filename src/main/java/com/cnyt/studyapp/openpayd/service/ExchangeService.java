package com.cnyt.studyapp.openpayd.service;

import com.cnyt.studyapp.openpayd.dto.TransactionDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Component
public interface ExchangeService {
    Double getExchangeRate(String sourceCurrency, String targetCurrency);
    TransactionDto getConversion(BigDecimal sourceAmount, String sourceCurrency, String targetCurrency);
    List<TransactionDto> getConversionList(Long transactionId, Date transactionDate, Integer page, Integer pageSize);
}
