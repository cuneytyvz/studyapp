package com.cnyt.studyapp.openpayd.service.impl;

import com.cnyt.studyapp.openpayd.dto.TransactionDto;
import com.cnyt.studyapp.openpayd.exception.TransactionNotFoundException;
import com.cnyt.studyapp.openpayd.model.Transaction;
import com.cnyt.studyapp.openpayd.repository.TransactionRepository;
import com.cnyt.studyapp.openpayd.service.ExchangeService;
import com.cnyt.studyapp.openpayd.service.external.RatesApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExchangeServiceImpl implements ExchangeService {
    @Autowired
    private RatesApiService ratesApiService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Double getExchangeRate(String sourceCurrency, String targetCurrency) {
        return ratesApiService.getExchangeRate(sourceCurrency,targetCurrency);
    }

    @Override
    public TransactionDto getConversion(BigDecimal sourceAmount, String sourceCurrency, String targetCurrency) {
        Double exchangeRate = ratesApiService.getExchangeRate(sourceCurrency,targetCurrency);
        sourceAmount = sourceAmount.setScale(4, RoundingMode.HALF_DOWN);
        BigDecimal targetAmount = sourceAmount.multiply(new BigDecimal(exchangeRate)).setScale(4, RoundingMode.HALF_DOWN);
        Transaction transaction = new Transaction(new Date(),sourceAmount,sourceCurrency,targetAmount,targetCurrency);
        transaction = transactionRepository.save(transaction);

        return transaction.toDto();
    }

    @Override
    public List<TransactionDto> getConversionList(Long transactionId, Date transactionDate,Integer page, Integer pageSize) {
        if(Objects.nonNull(transactionId)) {
            Transaction transaction = transactionRepository.findById(transactionId).orElseThrow(() -> new TransactionNotFoundException(transactionId));;
            return Arrays.asList(transaction.toDto());
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(transactionDate);
        cal.set(Calendar.HOUR_OF_DAY,00);
        cal.set(Calendar.MINUTE,00);
        cal.set(Calendar.SECOND,00);
        Date start = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,59);
        Date end = cal.getTime();
        return transactionRepository.findAllByDateBetween(start,end, PageRequest.of(page,pageSize)).stream().map(t -> t.toDto()).collect(Collectors.toList());
    }
}
