package com.cnyt.studyapp.openpayd.repository;

import com.cnyt.studyapp.openpayd.model.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
    List<Transaction> findAllByDateBetween(Date start, Date end, Pageable pageable);
}
