package com.cnyt.studyapp.openpayd.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransactionNotFoundException extends  RuntimeException{
    Logger logger = LoggerFactory.getLogger(TransactionNotFoundException.class);

    public TransactionNotFoundException(Long id) {
        super("Transaction is not found with id : " + id);
        logger.error("Transaction with id " + id + " could not be found.");
    }
}
