package com.cnyt.studyapp.openpayd.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SymbolNotFoundException extends  RuntimeException{
    Logger logger = LoggerFactory.getLogger(SymbolNotFoundException.class);

    public SymbolNotFoundException() {
        super("Source or target symbols are not correct.");
        logger.error("Given source or target symbol are not correct.");
    }
}
