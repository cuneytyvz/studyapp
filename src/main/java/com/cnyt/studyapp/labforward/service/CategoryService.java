package com.cnyt.studyapp.labforward.service;

import com.cnyt.studyapp.labforward.dto.CategoryDto;
import com.cnyt.studyapp.labforward.dto.ItemDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CategoryService {
    List<ItemDto> findItemsByCategory(Long categoryId);
    CategoryDto createCategory(CategoryDto categoryDto);
    ItemDto createItem(ItemDto itemDto);
    ItemDto updateItem(ItemDto itemDto);
}
