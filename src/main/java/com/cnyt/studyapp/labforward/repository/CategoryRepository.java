package com.cnyt.studyapp.labforward.repository;

import com.cnyt.studyapp.labforward.model.CategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryModel, Long> {
}
