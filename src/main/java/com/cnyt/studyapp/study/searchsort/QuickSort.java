package com.cnyt.studyapp.study.searchsort;

import java.util.Arrays;

public class QuickSort {
    public static void main(String []args){
        int[] arr = new int[]{7,2,6,5,4,100,8,1,7};
        quickSort(arr,0,8);
        System.out.println(Arrays.toString(arr));
    }

    static void quickSort(int[] arr, int left, int right) {
        // select pivot element (e.g last element)
        // put elements less than pivot element to left side of pivot
        // more than pivot element to right side of pivot
        // recursively repeat it for left and right sides

        // 7,2,6,5,4,100,8,1,7
        // 7,2,6,5,4,1,8,100,7
        if(left >= right) return;

        int pivot = arr[right];

        int indexLeft = left, indexRight = right - 1;
        while(indexLeft < indexRight) {
            for(int i = indexLeft; i < right; i++) {
                if(arr[i] > pivot) {
                    indexLeft = i;
                    break;
                }
            }
            for(int i = indexRight; i >= left; i--) {
                if(arr[i] < pivot) {
                    indexRight = i;
                    break;
                }
            }

            if(indexLeft < indexRight)
                swap(arr,indexLeft++,indexRight--);
        }

        swap(arr,right,indexLeft);

        quickSort(arr,left,indexLeft - 1);
        quickSort(arr,indexLeft + 1, right);
    }

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
