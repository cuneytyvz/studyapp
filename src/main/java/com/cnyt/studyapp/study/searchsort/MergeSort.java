package com.cnyt.studyapp.study.searchsort;

import java.util.Arrays;
import java.util.*;

public class MergeSort{

    static int[] memo = new int[8];
    static List<String> pals = new ArrayList<>();
    public static void main(String []args){
        int[] result = mergeSort(new int[]{7,2,6,5,4,100,8,1,7});
        System.out.println(Arrays.toString(result));
    }

    static int[] mergeSort(int[] arr)
    {
        return helper(arr,0,arr.length-1);
    }

    static int[] helper(int[] arr, int left, int right) {
        System.out.println(left + ", " + right);
        if(left > right) return new int[]{};
        if(left == right) return new int[]{arr[left]};

        int middle = (right + left) / 2;

        int[] arrLeft = helper(arr,left, middle);
        int[] arrRight = helper(arr,middle + 1, right);

        int i = 0, j = 0, k = 0;
        int[] result = new int[right - left + 1];
        while(i < arrLeft.length && j < arrRight.length) {
            if(arrLeft[i] < arrRight[j]) {
                result[k++] = arrLeft[i++];
            } else {
                result[k++] = arrRight[j++];
            }
        }

        while(i < arrLeft.length)
            result[k++] = arrLeft[i++];

        while(j < arrRight.length)
            result[k++] = arrRight[j++];

        return result;
    }
}
