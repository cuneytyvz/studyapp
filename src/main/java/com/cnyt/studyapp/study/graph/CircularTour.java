package com.cnyt.studyapp.study.graph;

public class CircularTour {
    static class PetrolPump {
        int amount;
        int distance;
        PetrolPump(int amount, int distance){
            this.amount = amount;
            this.distance = distance;
        }
    }

    static int find(PetrolPump[] pumps) {
        int start = 0, remaining = 0,index = 0;
        while(true) {
            remaining = pumps[index].amount - pumps[index].distance + remaining;
            if(start == (index+1)%pumps.length) break;
            if(remaining >= 0) {
                index = (index + 1) % pumps.length;
            }
            else {
                start = ++index;
                remaining = 0;
                if(start >= pumps.length) break;
            }
        }

        if(start >= pumps.length || remaining < 0)
            return -1;
        else
            return start;
    }

    // start     0  0 2  2  2  2  2  2
    // index     0  1 2  3  4  0  1  2
    // remaining 2 -1 4  5  1  3  0
    // remainin2 2 -1 4  5  0  2 -1

    // start     0  0 0  0  0  0  2  2
    // index     0  1 2  3  4  0  1  2
    // remaining 12 9 13 14 10

    // count     1  2 1  2  3  4
    // 6,4 - 3,6 - 7,3 - 2,1 - 2,6 && 2, -3, 4, 1,-4 && *,-1,3,4,0                                -14, -17, -13, -29
    // 4 - 10 - 13 - 14 - 20
    //
    public static void main(String[] args) {
        PetrolPump[] arr = {new PetrolPump(1, 4),
                new PetrolPump(33, 6),
                new PetrolPump(7, 3),
                new PetrolPump(2, 1),
                new PetrolPump(2, 6)};

        int start = find(arr);

        System.out.println(start == -1 ? "No Solution" : "Start = " + start); // 2
    }
}
