package com.cnyt.studyapp.study.graph;

public class PrintBottomViewOfATree {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static int[][] countMatrix = new int[][]{};
    static void print(Node x) {
        int level = 1, leftColumnCount = 0;
        Node temp = x;
        while(temp.left != null) {
            leftColumnCount++;
            temp = temp.left;
        }

        traverse(x, level, leftColumnCount+1);

    }

    static void traverse(Node node, int level, int column) {
        countMatrix[column][level] = node.key;

        traverse(node.left,level+1,column-1);
        traverse(node.right,level+1,column+1);
    }
    
    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(15);
        x.left = new Node(10);
        x.right = new Node(20);
        x.left.left = new Node(8);
        x.left.right = new Node(12);
        x.right.left = new Node(16);
        x.right.right = new Node(25);
        
        print(x);
    }
}
