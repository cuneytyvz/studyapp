package com.cnyt.studyapp.study.graph;

import java.util.HashMap;
import java.util.Map;

public class PreOrderInorderBinary {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
        }
    }

    static int preOrderIndex = 0;
    static Map<Integer,Integer> inOrderIndexMap = new HashMap<>();
    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        for(int i = 0; i < inorder.length; i++) {
            inOrderIndexMap.put(inorder[i],i);
        }

        return array2Tree(preorder,0,preorder.length-1);
    }

    static TreeNode array2Tree(int[] preorder, int left, int right) {
        if(left > right) return null;

        int rootValue = preorder[preOrderIndex++];
        TreeNode root = new TreeNode(rootValue);

        root.left = array2Tree(preorder,left,inOrderIndexMap.get(rootValue) - 1);
        root.right = array2Tree(preorder,inOrderIndexMap.get(rootValue) + 1, right);

        return root;
    }

    static void print(TreeNode node) {
        if(node != null) {
            System.out.print(node.val + " ");
            print(node.left);
            print(node.right);
        } else {
//            System.out.print("null ");
        }
    }
    public static void main(String[] args) {
        TreeNode node = buildTree(new int[]{3,9,20,15,7}, new int[]{9,3,15,20,7});
        print(node);

    }
}
