package com.cnyt.studyapp.study.graph;

import java.util.Stack;

public class FindKthDistantNodes {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static boolean findPath(Node root, Node n, Stack<Node> stack) {
        if(root == null) return false;
        if(root == n) return true;

        stack.push(root);
        boolean left = findPath(root.left,n,stack);
        boolean right = findPath(root.right,n,stack);

        if(!left && !right) {
            stack.pop();
            return false;
        }

        return true;
    }

    static void print(Node root, Node n, int k) {
        Stack<Node> stack = new Stack<>();

        findPath(root,n,stack);

        search(n.left,root,k,1);
        search(n.right,root,k,1);

        Node lastVisited = n;
        int distance = 1;
        while(!stack.isEmpty()) {
            Node parent = stack.pop();
            if(distance == k) {
                System.out.println(parent.key);
                break;
            }
            search(parent.left,lastVisited,k,distance);
            search(parent.right,lastVisited,k,distance);
            lastVisited = parent;
            distance++;
        }

    }

    static void search(Node root, Node lastVisited, int k,int distance) {
        if(root == null) return;
        if(root == lastVisited) return;

        if(distance == k) {
            System.out.println(root.key);
            return;
        }

        search(root.left,lastVisited,k,distance+1);
        search(root.right,lastVisited,k,distance+1);
    }
    
    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(3);
        x.left.left = new Node(4);
        x.left.right = new Node(5);
        x.right.left = new Node(6);
        x.right.right = new Node(7);
        x.left.left.left = new Node(8);
        x.left.left.right = new Node(9);
        x.left.left.left.left = new Node(10);
        x.left.left.left.right = new Node(11);

        print(x,x.left.left,3);
    }
}
