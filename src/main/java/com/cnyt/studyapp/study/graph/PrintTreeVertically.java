package com.cnyt.studyapp.study.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class PrintTreeVertically {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }

        @Override
        public String toString(){
            return key + "";
        }
    }

    static class Data {
        Node node;
        int distance;

        Data(Node n, int d) {
            node = n;
            distance = d;
        }
    }

    static TreeMap<Integer, List<Node>> map = new TreeMap<>();
    static void print(Node root) {
        traverse(root,0);

        for(List<Node> nodes : map.values()) {
            System.out.println(nodes);
        }
    }

    static void traverse(Node node, int distance) {
        if(node == null) return;

        map.computeIfAbsent(distance, k-> new ArrayList<>()).add(node);

        traverse(node.left,distance-1);
        traverse(node.right, distance+1);
    }
    
    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(3);
        x.left.left = new Node(4);
        x.left.right = new Node(5);
        x.right.left = new Node(6);
        x.right.right = new Node(7);
//        x.left.left.left = new Node(8);
//        x.left.left.right = new Node(9);
//        x.left.left.left.left = new Node(10);
//        x.left.left.left.right = new Node(11);

        print(x);
    }
}
