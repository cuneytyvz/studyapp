package com.cnyt.studyapp.study.graph;

public class SpiralOrder {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static int height(Node root) {
        if(root == null) return 0;

        int lHeight = height(root.left);
        int rHeight = height(root.right);

        return Math.max(lHeight,rHeight)+1;
    }

    static void print(Node root) {
        int n = height(root);
        for(int i = 0; i < n; i++) {
            int level = i+1;
            if(level % 2 == 0) {
                printLeft2Right(root,level);
            } else {
                printRight2Left(root,level);
            }
        }
    }

    static void printLeft2Right(Node root, int level) {
        if(root == null) return;

        if(level == 1) {
            System.out.print(root.key + " ");
            return;
        }

        printLeft2Right(root.left,level-1);
        printLeft2Right(root.right,level-1);
    }

    static void printRight2Left(Node root, int level) {
        if(root == null) return;

        if(level == 1) {
            System.out.print(root.key + " ");
            return;
        }

        printRight2Left(root.right,level-1);
        printRight2Left(root.left,level-1);
    }

    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(3);
        x.left.left = new Node(4);
        x.left.right = new Node(5);
        x.right.left = new Node(6);
        x.right.right = new Node(7);
        x.left.left.left = new Node(8);
        x.left.left.right = new Node(9);
        x.left.left.left.left = new Node(10);
        x.left.left.left.right = new Node(11);

        print(x);
    }
}
