package com.cnyt.studyapp.study.graph;

public class SumOfNodesOfMaxDepth {

    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static int sum(Node root) {
        return 0;
    }
    
    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(3);
        x.left.left = new Node(4);
        x.left.right = new Node(5);
        x.right.left = new Node(6);
        x.right.right = new Node(7);
        x.left.left.left = new Node(8);
        x.left.left.right = new Node(9);
        x.left.left.left.left = new Node(10);
        x.left.left.left.right = new Node(11);

        System.out.println(sum(x));
    }
}
