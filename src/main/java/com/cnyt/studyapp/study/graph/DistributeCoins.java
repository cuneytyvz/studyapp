package com.cnyt.studyapp.study.graph;

class DistributeCoins {
    //  Definition for a binary tree node.
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    static int count = 0;
    public int distributeCoins(TreeNode root) {
        traverse(new MyNode(root,null));
        return count;
    }

    void traverse(MyNode myNode) {
        TreeNode root = myNode.current;

        if(root == null) return;

        traverse(new MyNode(root.left,root));
        traverse(new MyNode(root.right,root));

        if(root.val > 0 && root.left != null && root.left.val == 0) {
            root.val--;
            root.left.val++;
            count++;
        }

        if(root.val > 0 && root.right != null && root.right.val == 0) {
            root.val--;
            root.right.val++;
            count++;
        }

        if(root.val > 1 && myNode.parent != null) {
            myNode.parent.val += root.val - 1;
            count += root.val - 1;
            root.val = 1;
        }
    }

    class MyNode {
        TreeNode parent;
        TreeNode current;
        MyNode(){}
        MyNode(TreeNode current, TreeNode parent) {
            this.current = current;
            this.parent = parent;
        }
    }

    static void print(TreeNode node) {
        if(node != null) {
            System.out.print(node.val + " ");
            print(node.left);
            print(node.right);
        } else {
            System.out.print("null ");
        }
    }
    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(0);
        node.left.left = new TreeNode(0);
        node.right = new TreeNode(0);
        node.right.right = new TreeNode(5);
        node.right.right.left = new TreeNode(0);
        int result = new DistributeCoins().distributeCoins(node);
        System.out.println(result);
        print(node);
    }
}
