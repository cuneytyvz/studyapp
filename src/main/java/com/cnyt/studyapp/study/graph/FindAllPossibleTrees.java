package com.cnyt.studyapp.study.graph;

public class FindAllPossibleTrees {

    static int[][] find(int[] inorder) {
        return null;
    }

    public static void main(String[] args) {
        int[][] result = find(new int[]{4, 5, 7});
    }
}

// 4, 5, 7, 3, 2

// 4 3 7 5
//   5
// 4   7
//       3
//         2

// 4 5 3 7
//   7
//  5  3
// 4

// 4 5 3 7
//   7
// 4   3
//   5

// 5 3 7 4
//  4
//     7
//   5   3

// 3 7 5 4
//  4
//    5
//      7
//        3

