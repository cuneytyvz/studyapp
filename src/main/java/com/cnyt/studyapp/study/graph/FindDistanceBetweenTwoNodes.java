package com.cnyt.studyapp.study.graph;

public class FindDistanceBetweenTwoNodes {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static boolean foundN = false, foundM = false;
    static int depthN = 1, depthM = 1, depthAncestor = 1;
    static int find(Node root,int n, int m) {
        helper(root,n,m,1);
        return (depthN-depthAncestor) + (depthM-depthAncestor);
    }

    static Integer helper(Node root, int n, int m, int level) {
        if(root == null) return null;

        if(root.key == n) {
            foundN = true;
            depthN = level;
        }
        if(root.key == m) {
            foundM = true;
            depthM = level;
        }

        Integer left = helper(root.left,n,m,level+1);
        if(foundM && foundN) return left;
        Integer right = helper(root.right,n,m,level+1);

        if(left != null && right != null) {
            depthAncestor = level;
            return root.key;
        }

        if(left == null) return right;
        if(right == null) return left;

        return null;
    }

    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(3);
        x.left.left = new Node(4);
        x.left.right = new Node(5);
        x.right.left = new Node(6);
        x.right.right = new Node(7);
        x.left.left.left = new Node(8);
        x.left.left.right = new Node(9);
        x.left.left.left.left = new Node(10);
        x.left.left.left.right = new Node(11);

        System.out.println(find(x,11,1));
    }
}
