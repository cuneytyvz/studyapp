package com.cnyt.studyapp.study.graph;

public class BoundaryTraversal {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }
    }

    static void traverse(Node root) {
        helper(root,false,false);
    }

    static void helper(Node root, boolean isThereLeft, boolean isThereRight) {
        if(root == null) return;

        if((!isThereLeft && isThereRight)) {
            System.out.print(root.key + " ");
            helper(root.left,isThereLeft,root.right != null);
            helper(root.right,root.left != null,isThereRight);
        } else if(root.left == null && root.right != null) {
            System.out.print(root.key + " ");
            helper(root.right,isThereLeft,isThereRight);
        } else if(root.left != null && root.right == null) {
            helper(root.left,isThereLeft,isThereRight);
            System.out.print(root.key + " ");
        } else if(root.left == null && root.right == null) {
            System.out.print(root.key + " ");
        } else if (!isThereLeft && !isThereRight ) {
            System.out.print(root.key + " ");
            helper(root.left,isThereLeft,root.right != null);
            helper(root.right,root.left != null,isThereRight);
        }  else if (isThereLeft && !isThereRight) {
            helper(root.left,isThereLeft,root.right!=null);
            helper(root.right,root.left!=null,isThereRight);
            System.out.print(root.key + " ");
        } else {
            helper(root.left,isThereLeft,root.right != null);
            helper(root.right,root.left != null,isThereRight);
        }
    }

    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(20);
        x.left = new Node(8);
        x.right = new Node(22);
        x.left.left = new Node(4);
        x.left.left.left = new Node(5);
        x.left.right = new Node(12);
        x.right.right = new Node(25);
        x.left.right.right = new Node(10);
        x.left.right.right.left = new Node(11);
//        x.left.right.right = new Node(14);

        traverse(x);
    }
}
