package com.cnyt.studyapp.study.graph;

import java.util.*;

public class Itinerary {
    public List<String> findItinerary(List<List<String>> tickets) {
        HashMap<String, List<String>> map = new HashMap<>();

        for(List<String> list : tickets) {
            if(map.containsKey(list.get(0))) {
                map.get(list.get(0)).add(list.get(1));
            } else {
                map.put(list.get(0),new ArrayList<String>(Arrays.asList(list.get(1))));
            }
        }

        List<String> jfk = map.get("JFK");
        return traverse(map,"JFK");
    }

    List<String> traverse(HashMap<String, List<String>> map, String node) {
        if(!map.containsKey(node))
            return Arrays.asList(node);

        List<String> dests = map.get(node);

        List<List<String>> list = new ArrayList<>();
        for(String dest : dests) {
            List<String> l = new ArrayList<>();
            l.add(node);
            l.addAll(traverse(map,dest));

            list.add(l);
        }

        List<String> smallest = list.get(0);
        for(List<String> paths : list) {

        }

        return list.get(0);
    }

    public static void main(String[] args) {
        List<List<String>> input = input2();

        List<String> output = new Itinerary().findItinerary(input);

        System.out.println(output);
    }

    static List<List<String>> input1() {
        List<List<String>> input = new ArrayList<>();
        input.add(Arrays.asList("MUC","LHR"));
        input.add(Arrays.asList("JFK","MUC"));
        input.add(Arrays.asList("SFO","SJC"));
        input.add(Arrays.asList("LHR","SFO"));

        return input;
    }

    static List<List<String>> input2() {
        List<List<String>> input = new ArrayList<>();
        List<String> l1 = new ArrayList<>();
        input.add(Arrays.asList("JFK","SFO"));
        input.add(Arrays.asList("JFK","ATL"));
        input.add(Arrays.asList("SFO","ATL"));
        input.add(Arrays.asList("ATL","JFK"));
        input.add(Arrays.asList("ATL","SFO"));

        return input;
    }

}
