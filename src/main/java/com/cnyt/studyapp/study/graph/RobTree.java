package com.cnyt.studyapp.study.graph;

import java.util.HashMap;
import java.util.Map;

public class RobTree {
    static class Node
    {
        int key;
        Node left = null, right = null;

        Node(int key) {
            this.key = key;
        }

        @Override
        public int hashCode(){
//            if(left != null && right != null) return key + left.key + right.key;
//            if(left != null) return key + left.key;
//            if(right != null) return key + right.key;
            return key;
        }
    }
    
    static class Data {
        int robbed, notrobbed;
        Data(int r, int n) {
            robbed = r;
            notrobbed = n;
        }
    }
    
    static int rob(Node root) {
        Map<Node, Data> map = new HashMap<>();

        Data d = traverse(root,map);

        return Math.max(d.robbed,d.notrobbed);
    }

    static Data traverse(Node root, Map<Node,Data> map) {
        if(root == null) return new Data(0,0);

        Data left = traverse(root.left,map);
        Data right = traverse(root.right,map);

        int notRobbed = left.robbed + right.robbed;
        int robbed = root.key + map.getOrDefault(root.left,new Data(0,0)).notrobbed
                + map.getOrDefault(root.right,new Data(0,0)).notrobbed;

        Data d = new Data(robbed,notRobbed);
        map.put(root,d);

        return d;
    }

    public static void main(String[] args) {
        // construct the first tree
        Node x = new Node(1);
        x.left = new Node(2);
        x.right = new Node(30);
        x.right.left = new Node(5);
        x.left.left = new Node(4);
        x.left.right = new Node(30);
        x.left.right.left = new Node(7);

        int result = rob(x);
        System.out.println(result);
    }
}
//          1
//       2      30
//     4   30  5
//        7