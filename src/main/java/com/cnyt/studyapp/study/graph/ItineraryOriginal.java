package com.cnyt.studyapp.study.graph;

import java.util.*;

public class ItineraryOriginal {

    static Map<String, PriorityQueue<String>> targets = new HashMap<>();
    static List<String> route = new LinkedList();

//    [JFK, ATL, JFK, SFO, ATL, SFO]
    public static List<String> findItinerary(String[][] tickets) {
        for (String[] ticket : tickets)
            targets.computeIfAbsent(ticket[0], k -> new PriorityQueue()).add(ticket[1]);
        visit("JFK");
        return route;
    }

    static void visit(String airport) {
        while(targets.containsKey(airport) && !targets.get(airport).isEmpty())
            visit(targets.get(airport).poll());
        route.add(0, airport);
    }

    public static void main(String[] args) {
//        System.out.println(findItinerary(input1()));
        System.out.println(findItinerary(input2()));
    }

    static String[][] input1() {
        String[][] input = new String[][] {
                {"MUC","LHR"},
                {"JFK","MUC"},
                {"SFO","SJC"},
                {"LHR","SFO"}
        };

        return input;
    }

    static String[][] input2() {
        String[][] input = new String[][] {
                {"JFK","SFO"},
                {"JFK","ATL"},
                {"SFO","ATL"},
                {"ATL","JFK"},
                {"ATL","SFO"}
        };

        return input;
    }
}
