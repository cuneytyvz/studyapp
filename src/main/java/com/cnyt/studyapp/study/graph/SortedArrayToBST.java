package com.cnyt.studyapp.study.graph;

import java.util.Arrays;

public class SortedArrayToBST {
    public static int[] sortedArrayToBST(int[] nums)
    {
        return helper(nums,0,nums.length-1);
    }

    static int[] helper(int[] nums, int l, int r) {
        if(l>r) return null;

        int mid = (l+r)/2;

        int[] left = helper(nums,l,mid-1);
        int[] right = helper(nums,mid+1,r);

        int leftSize = left == null ? 0 : left.length;
        int rightSize = right == null ? 0 : right.length;

        int[] result = new int[leftSize+rightSize+1];
        result[0]=nums[mid];
        int i;
        for(i=0;i<leftSize;i++){
            result[i+1]=left[i];
        }
        for(i=i;i<rightSize+leftSize;i++){
            result[i+1]=right[i-leftSize];
        }

        return result;
    }

    public static void main(String[] args) {
        int [] result = sortedArrayToBST(new int[]{1,2,3,4,5,6,7});
        System.out.println(Arrays.toString(result));
    }
}
