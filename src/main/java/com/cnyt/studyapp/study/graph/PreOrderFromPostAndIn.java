package com.cnyt.studyapp.study.graph;
import java.util.*;

public class PreOrderFromPostAndIn {

    //             1
    //        2       3
    //    4       5       6
    public static void main(String []args){
        int[] result = toPreOrder(new int[]{4, 5, 2, 6, 3, 1},new int[]{4, 2, 5, 1, 3, 6}); // 1 2 4 5 3 6
        System.out.println(Arrays.toString(result));
    }

    static Stack<Integer> stack = new Stack<>();
    static int postOrderIndex;

    static int[] toPreOrder(int[] post, int[] in) {
        int n = post.length;
        postOrderIndex = n - 1;

        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < n; i++)
            map.put(in[i],i);

        helper(post,0,n-1,map);

        int[] result = new int[n];
        int i = 0;
        while(!stack.isEmpty())
            result[i++] = stack.pop();

        return result;
    }

    static void helper(int[] post, int left, int right, Map<Integer,Integer> map) {
        if(left > right) return;

        int val = post[postOrderIndex--];
        int rootIndex = map.get(val);

        helper(post,rootIndex+1,right,map);
        helper(post,left,rootIndex-1,map);

        stack.push(val);
    }
}
