package com.cnyt.studyapp.study.graph;

import java.util.ArrayList;
import java.util.List;

public class FindSmallestSubtree {

    static class Node
    {
        int data;
        Node left, right;

        public Node(int item)
        {
            data = item;
            left = right = null;
        }
    }

    //Root of the Binary Tree
    static Node root;

    static List<Node> path1 = new ArrayList<>();
    static List<Node> path2 = new ArrayList<>();

    static Node find(Node root, int val1, int val2) {
        if(!traverse(root,val1,path1)) return null;
        if(!traverse(root,val2,path2)) return null;

        for(int i = path1.size() - 1; i >= 0; i--) {
            for(int j = path2.size() - 1; j >= 0; j--) {
                if(path1.get(i).data == path2.get(j).data) {
                    return path1.get(i);
                }
            }
        }

        return null;
    }

    static boolean traverse(Node root, int val, List<Node> path) {
        if(root == null) return false;

        path.add(root);

        if(val == root.data) {
            return true;
        }

        if(val < root.data && traverse(root.left,val,path)) return true;
        if(val > root.data && traverse(root.right,val,path)) return true;

        path.remove(path.size() - 1);

        return false;
    }

    public static void main(String args[])
    {
        FindSmallestSubtree tree = new FindSmallestSubtree();
        tree.root = new Node(4);
        tree.root.left = new Node(2);
        tree.root.right = new Node(5);
        tree.root.left.left = new Node(1);
        tree.root.left.right = new Node(3);

        Node result = find(tree.root,1,3);

        System.out.println(result.data);
    }
}
