package com.cnyt.studyapp.study.graph;

public class MaximumPathSum {
    static int maxValue = Integer.MIN_VALUE;
    public static int maxPathSum(PreOrderInorderBinary.TreeNode root) {
        maxPathDown(root);
        return maxValue;
    }

    static int maxPathDown(PreOrderInorderBinary.TreeNode node) {
        if(node == null) return 0;

        int left = Math.max(0, maxPathDown(node.left));
        int right = Math.max(0, maxPathDown(node.right));
        maxValue = Math.max(maxValue, left + right + node.val);
        return Math.max(left,right) + node.val;
    }

    public static void main(String[] args) {
        PreOrderInorderBinary.TreeNode node = PreOrderInorderBinary.buildTree(new int[]{-10,9,20,15,7}, new int[]{9,-10,15,20,7});

        System.out.println(maxPathSum(node));
    }
}
