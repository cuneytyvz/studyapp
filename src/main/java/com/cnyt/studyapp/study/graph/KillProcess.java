package com.cnyt.studyapp.study.graph;

import java.util.*;

public class KillProcess {
    static List<Integer> find(int[] pIds, int[] parents, int kill) {
        Map<Integer,Node> map = new HashMap<>();
        for(int id : pIds)
            map.put(id,new Node(id));

        for(int i = 0; i < parents.length; i++) {
            int parent = parents[i];
            int process = pIds[i];
            if(parent == 0) continue;
            map.get(parent).childs.add(map.get(process));
        }

        Node root = map.get(kill);

        List<Integer> result = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            Node n = queue.poll();
            queue.addAll(n.childs);

            n.childs.stream().map(nn -> nn.pId).forEach(result::add);
        }

        return result;
    }

    public static void main(String[] args) {
        List<Integer> result = find(new int[]{1,2,3,4,5,6,7,8,9}, new int[]{2,0,2,3,3,3,3,4,5}, 3);
        System.out.println(result);
    }
    // 2 -> 1 ->
    //   -> 3 -> 4 -> 8
    //        -> 5 -> 9
    //        -> 6
    //        -> 7
    static class Node {
        int pId;
        Node parent;
        List<Node> childs = new ArrayList<>();

        Node(int id) {
            pId = id;
        }
    }
}
