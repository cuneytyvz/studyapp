package com.cnyt.studyapp.study.linkedlist;

import java.util.ArrayList;
import java.util.List;

public class SubtractOne {
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    static ListNode subtract(ListNode head) {
        ListNode last = head, prev = null;
        List<ListNode> list = new ArrayList<>();
        while(last.next != null) {
            if(last.val != 0)
                prev = last;
            last = last.next;
            if(last.val != 0 && last.next == null && !list.isEmpty()) {
                list = list.subList(list.size() - 1,list.size());
            } else if(last.val != 0 && last.next != null) {
                list.clear();
            } else if(last.val == 0) {
                list.add(last);
            }
        }

        if(last.val != 0) {
            last.val--;
            return head;
        }

        for(ListNode n : list) {
            n.val=9;
        }

        prev.val--;

        if(head.val == 0)
            return head.next;
        else
            return head;
    }

    static ListNode recursiveSubtract(ListNode head) {
        if(helper(head.next)) {
            head.val--;
        }

        if(head.val == 0)
            return head.next;
        else
            return head;
    }

    static boolean helper(ListNode head) {
        if(head.next == null) {
            if(head.val == 0) {
                head.val = 9;
                return true;
            } else {
                head.val--;
                return false;
            }
        } else {
            if(helper(head.next)) {
                if(head.val == 0) {
                    head.val = 9;
                    return true;
                } else {
                    head.val--;
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    static void print(ListNode head) {
        System.out.print(head.val);
        while(head.next != null) {
            head = head.next;
            System.out.print(" -> " + head.val);
        }
        System.out.println();
    }

    // last = 0, prev = 3
    // 1 - 2 - 3 - 0
    // 1 - 2 - 2 - 9

    // 1 - 0 - 0 - 0
    // 0 - 9 - 9 - 9
    public static void main(String[] args) {
        ListNode node4 = new ListNode(0);
        ListNode node3 = new ListNode(1,node4);
        ListNode node2 = new ListNode(0,node3);
        ListNode node1 = new ListNode(2,node2);

        ListNode result = recursiveSubtract(node1);

        print(result);
    }
}
