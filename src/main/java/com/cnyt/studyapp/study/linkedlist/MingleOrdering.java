package com.cnyt.studyapp.study.linkedlist;

public class MingleOrdering {
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    static ListNode mingle(ListNode node) {
        ListNode curr=node,next = null;

        while(curr.next != null) {
            next = curr.next;
            ListNode last = curr, prev = null;
            while(last.next != null) {
                prev = last;
                last=last.next;
            }
            last.next = next;
            curr.next = last;
            prev.next = null;
            curr = next;
        }

        return node;
    }

    static ListNode mingleRecursive(ListNode node) {

        ListNode curr = node, next=curr.next, last=node, prev=null;
        while(last.next != null) {
            prev = last;
            last = last.next;
        }

        curr.next=last;
        last.next=next;
        prev.next=null;

        if(next.next != null)
            mingleRecursive(next);

        return node;
    }

    static void print(ListNode head) {
        System.out.print(head.val);
        while(head.next != null) {
            head = head.next;
            System.out.print(" -> " + head.val);
        }
        System.out.println();
    }

    // curr = 1, next = 2, 1.next = 7, 7.next = next(2), 6.next=null
    // curr = 2, next = 3, 2.next = 6, 6.next = next(3), 5.next=null
    // curr = 3, next = 4, 3.next =5, 5.next= next(4), 4.next=null
    // curr = 4, next = null, break
    // 1 - 2 - 3 - 4 - 5 - 6 - 7
    // 1 - 7 - 2 - 6 - 3 - 5 - 4
    //
    public static void main(String[] args) {
//        ListNode node7 = new ListNode(7);
//        ListNode node6 = new ListNode(6,node7);
        ListNode node5 = new ListNode(5);
        ListNode node4 = new ListNode(4,node5);
        ListNode node3 = new ListNode(3,node4);
        ListNode node2 = new ListNode(2,node3);
        ListNode node1 = new ListNode(1,node2);

        ListNode result = mingleRecursive(node1);

        print(result);
    }
}
