package com.cnyt.studyapp.study.dynamic;

import java.util.Arrays;

public class CovidEviction {
    static int[][] m;
    static int evict(int[][] m) {
        int count = 0;

        for(int i = 0; i < m.length; i++) {
            for(int j = 0; j < m[0].length; j++) {
                if(i==0 && j==0) {
                    if((m[i+1][j] == 1 || m[i][j+1] == 1) && m[i][j] == 1) {
                        count++;
                        m[i][j] = 0;
                    }
                } else if(i==0) {
                    if(m[i][j-1] == 1 && m[i][j] == 1) {
                        count++;
                        m[i][j] = 0;
                    }
                } else if(j==0) {
                    if(m[i-1][j] == 1 && m[i][j] == 1) {
                        count++;
                        m[i][j] = 0;
                    }
                } else {
                    if((m[i][j-1] == 1 || m[i-1][j] == 1) && m[i][j] == 1)  {
                        count++;
                        m[i][j] = 0;
                    }
//                    if(i != m.length-1 && m[i+1][j] == 1) {
//                        count++;
//                        m[i][j] = 0;
//                    }
                }

            }
        }

        return count;
    }

    public static void main(String[] args) {
        m = new int[][] {
                {1, 1, 0},
                {1, 1, 1},
                {1, 1, 1}
        };

        System.out.println(evict(m));
        for(int[] a : m) {
            System.out.println(Arrays.toString(a));
        }
    }
}
