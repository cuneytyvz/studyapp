package com.cnyt.studyapp.study.dynamic;

import java.util.Arrays;

public class LongestPalindromicSubstring {

    static String[][] memo;
    static String longestPalindrom(String str) {
        memo = new String[str.length()][str.length()];
        return helper(str,0,str.length() - 1);
    }

    // asracedard
    static String helper(String str, int left, int right) {
        if(left > right) return "";
        if(left == right) return memo[left][right] = str.substring(left,left+1);

        if(str.charAt(left) == str.charAt(right)) {
            String pal = helper(str,left+1,right-1);
            if(right - left - 1 == pal.length())
                return memo[left][right] = str.substring(left,right+1);
            else
                return memo[left][right] = pal;
        }

        String leftPal = helper(str,left,right-1);
        String rightPal = helper(str,left+1,right);

        if(leftPal.length() > rightPal.length()) {
            return memo[left][right] = leftPal;
        } else {
            return memo[left][right] = rightPal;
        }
    }

    public static void main(String[] args) {
        String result = longestPalindrom("asracecardbabayaro");
        for(String[] arr : memo) {
            System.out.println(Arrays.toString(arr));
        }
        System.out.println(result);
    }
}
