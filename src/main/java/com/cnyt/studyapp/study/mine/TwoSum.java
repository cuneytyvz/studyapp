package com.cnyt.studyapp.study.mine;

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
    public static int[] twoSum2(int[] nums, int target) {
        int[] result = new int[2];
        for(int i = 0; i < nums.length; i++) {
            for(int j=i+1; j<nums.length; j++) {
                if(nums[i] + nums[j] == target)
                    result = new int[]{i,j};
            }
        }

        return result;
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        HashMap<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            map.put(nums[i],i);
        }

        for(int i = 0; i < nums.length; i++) {
            if(map.containsKey(target - nums[i])) {
                result = new int[]{i, map.get(target-nums[i])};
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2,7,11,15};
        int[] output = twoSum(nums,17);

        System.out.println(Arrays.toString(output));
    }
}
