package com.cnyt.studyapp.study.mine;

public class BuySellStock {
    public static int maxProfit(int[] prices) {
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice)
                minprice = prices[i];
            else if (prices[i] - minprice > maxprofit)
                maxprofit = prices[i] - minprice;
        }
        return maxprofit;
    }

    public static void main(String[] args) {
        int[] prices = new int[]{7,2,5,3,6,4,9,1,10};

        System.out.println(maxProfit(prices));
    }
}
