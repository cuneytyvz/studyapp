package com.cnyt.studyapp.study.mine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ThreeSum {
    public static List<List<Integer>> threeSum(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            map.put(nums[i],i);
        }

        List<List<Integer>> result = new ArrayList<>();
        for(int i = 0; i < nums.length; i++) {
            List<Integer> list = twoSum(nums, -nums[i],map,i);
            if(!list.isEmpty()) {
                list.add(nums[i]);
                result.add(list);
            }
        }

        return result;
    }

    public static List<Integer> twoSum(int[] nums, int target, HashMap<Integer,Integer> map, int j) {
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i < nums.length; i++) {
            int complement = target - nums[i] ;
            if(map.containsKey(complement) && map.get(complement) != i && map.get(complement) != j && i != j) {
                result = new ArrayList<Integer>(Arrays.asList(nums[i],complement));
                break;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums = {-1,0,1,2,-1,-4,5};
        List<List<Integer>> output = threeSum(nums);

        for(List<Integer> list : output) {
            System.out.println(list);
        }
    }
}
