package com.cnyt.studyapp.study.mine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Amazon {

    public static void main(String[] args) {
        List<Integer> priceOfJeans = Arrays.asList(2,3,1);
        List<Integer> priceOfShoes = Arrays.asList(4);
        List<Integer> priceOfSkirts = Arrays.asList(2,3);
        List<Integer> priceOfTops = Arrays.asList(1,2);
        int dollars = 10;
        long count = getNumberOfOptions(priceOfJeans,priceOfShoes,priceOfSkirts,priceOfTops,dollars);
        System.out.println(count);
    }

    public static long getNumberOfOptions(List<Integer> priceOfJeans, List<Integer> priceOfShoes, List<Integer> priceOfSkirts, List<Integer> priceOfTops, int dollars) {
        List<List<Integer>> prices = new ArrayList<>();
        prices.add(priceOfJeans);
        prices.add(priceOfShoes);
        prices.add(priceOfSkirts);
        prices.add(priceOfTops);

        long count = check(prices,0,dollars);

        return count;
    }

    private static long check(List<List<Integer>> prices, int index, int dollars) {
        if(index == prices.size())
            return 0;

        int count = 0;
        for(Integer price : prices.get(index)) {
            if(dollars - price > 0 && index < prices.size() - 1) {
                count += check(prices,index+1,dollars - price);
            } else if(dollars - price >= 0 && index == prices.size() - 1) {
                count++;
            }
        }

        return count;
    }
}
