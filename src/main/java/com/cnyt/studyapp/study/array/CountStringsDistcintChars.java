package com.cnyt.studyapp.study.array;

import java.util.HashMap;
import java.util.Map;

public class CountStringsDistcintChars {
    // Function to count number of substrings
// having atleast k distinct characters
    static void atleastkDistinctChars(String s, int k)
    {

        // Stores the size of the string
        int n = s.length();

        // Initialize a HashMap
        Map<Character, Integer> mp = new HashMap<>();

        // Stores the start and end
        // indices of sliding window
        int begin = 0, end = 0;

        // Stores the required result
        int ans = 0;

        // Iterate while the end
        // pointer is less than n
        while (end < n) {

            // Include the character at
            // the end of the window
            char c = s.charAt(end);
            mp.put(c,mp.getOrDefault(c,0)+1);

            // Increment end pointer by 1
            end++;

            // Iterate until count of distinct
            // characters becomes less than K
            while (mp.size() >= k) {

                // Remove the character from
                // the beginning of window
                char pre = s.charAt(begin);
                mp.put(pre,mp.getOrDefault(pre,0)-1);

                // If its frequency is 0,
                // remove it from the map
                if (mp.get(pre)==0){
                    mp.remove(pre);
                }

                // Update the answer
                ans += s.length() - end + 1;
                begin++;
            }
        }

        // Print the result
        System.out.println(ans);
    }

    // Driver code
    public static void main (String[] args)
    {

        // Given inputs
        String S = "abcdca";
        int K = 3;
        atleastkDistinctChars(S, K);

    }
}

// abcdca
// abc* abcd abcdc abcdca
// bcd* bcdc* bcdca
// cdc cdca*
// dca*