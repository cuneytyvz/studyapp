package com.cnyt.studyapp.study.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) map.put(nums[i],i);
        for(int i = 0; i < nums.length; i++) {
            if(map.containsKey(target - nums[i])) return new int[]{i,map.get(target - nums[i])};
        }
        return new int[]{};
    }

    public static void main(String[] args) {
        int[] result = twoSum(new int[] {2,7,11,15},26);
        System.out.println(Arrays.toString(result));
    }
}
