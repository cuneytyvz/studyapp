package com.cnyt.studyapp.study.array;

import java.util.Arrays;

public class SumFarthestFromZero {
    static int[] sum(int[] arr) {
        int maxA = Integer.MIN_VALUE, maxB = Integer.MIN_VALUE, minA = Integer.MAX_VALUE, minB = Integer.MAX_VALUE;

        for(int n : arr) {
            if(n > maxA) {
                maxB = maxA;
                maxA = n;
            } else if(n > maxB) {
                maxB = n;
            }

            if(n < minA) {
                minB = minA;
                minA = n;
            } else if(n < minB) {
                minB = n;
            }
        }

        if(Math.abs(minA+minB) > maxA + maxB) {
            return new int[]{minA,minB};
        } else {
            return new int[]{maxA,maxB};
        }
    }

    public static void main(String[] args) {
        int[] result = sum(new int[]{
                10,2,100,-50,50,-50
        });
        System.out.println(Arrays.toString(result));
    }
}
