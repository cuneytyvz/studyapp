package com.cnyt.studyapp.study.array;

import java.util.Map;
import java.util.TreeMap;

public class PrintLeftGreater {

    static void print(int[] arr) {
        TreeMap<Integer,Integer> map = new TreeMap<>();
        for(int i = 0; i < arr.length; i++)
            map.put(arr[i],i);

        for(int i = 0; i < arr.length; i++) {
            int n = arr[i];
            System.out.print(n + ": ");
            Map.Entry<Integer,Integer> e = map.higherEntry(n);
            while(e != null) {
                if(e.getValue() < i)
                    System.out.print(e.getKey() + " ");
                e = map.higherEntry(e.getKey());
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        print(new int[]{5, 3, 9, 0, 16, 12});
    }
}
