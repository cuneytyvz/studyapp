package com.cnyt.studyapp.study.array;

public class MaxSquareSubmatrix {
    static int find(int[][] m) {
        return 0;
    }

    public static void main(String[] args) {
        int result = find(new int[][]{
                {1,1,1},
                {1,2,1},
                {1,1,2}
        });

        System.out.println(result);

    }
}

//        {0,1,1,0,1},
//        {1,1,1,1,1},
//        {0,1,1,1,1},
//        {1,0,1,1,1},
//        {1,1,1,1,1},
//        {0,0,0,0,0}
