package com.cnyt.studyapp.study.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FindSmallestSubset {
    // Function to split array elements
    // into two subsets having sum of
    // the smaller subset maximized
    static void findSubset(int[] arr)
    {
        // Stores the size of the array
        int N = arr.length;

        // Stores the frequency
        // of array elements
        Map<Integer, Integer> map
                = new HashMap<>();

        // Stores the total
        // sum of the array
        int totSum = 0;

        // Stores the sum of
        // the resultant set
        int s = 0;

        // Stores if it is possible
        // to split the array that
        // satisfies the conditions
        int flag = 0;

        // Stores the elements
        // of the first subset
        ArrayList<Integer> ans
                = new ArrayList<>();

        // Traverse the array arr[]
        for (int i = 0;
             i < arr.length; i++) {

            // Increment total sum
            totSum += arr[i];

            // Increment count of arr[i]
            map.put(arr[i],
                    map.getOrDefault(
                            arr[i], 0)
                            + 1);
        }

        // Sort the array arr[]
        Arrays.sort(arr);

        // Stores the index of the
        // last element of the array
        int i = N - 1;

        // Traverse the array arr[]
        while (i >= 0) {

            // Stores the frequency
            // of arr[i]
            int frq = map.get(arr[i]);

            // If frq + ans.size() is
            // at most remaining size
            if ((frq + ans.size())
                    < (N - (frq + ans.size()))) {

                for (int k = 0; k < frq; k++) {

                    // Append arr[i] to ans
                    ans.add(arr[i]);

                    // Decrement totSum by arr[i]
                    totSum -= arr[i];

                    // Increment s by arr[i]
                    s += arr[i];

                    i--;
                }
            }

            // Otherwise, decrement i
            // by frq
            else {
                i -= frq;
            }

            // If s is greater
            // than totSum
            if (s > totSum) {

                // Mark flag 1
                flag = 1;
//                break;
            }
        }

        // If flag is equal to 1
        if (flag == 1) {

            // Print the arrList ans
            for (i = ans.size() - 1;
                 i >= 0; i--) {

                System.out.print(
                        ans.get(i) + " ");
            }
        }

        // Otherwise, print "-1"
        else {
            System.out.print(-1);
        }
    }

    // Driver Code
    public static void main(String[] args)
    {
//        int[] arr = { 5, 3, 2, 4, 1, 2 };
        int[] arr = {10,10,10,10,10,20, 15, 20, 30, 20,20,20};
        findSubset(arr);
    }
}
