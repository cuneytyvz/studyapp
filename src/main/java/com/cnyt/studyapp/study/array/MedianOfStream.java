package com.cnyt.studyapp.study.array;

import java.util.Collections;
import java.util.PriorityQueue;

public class MedianOfStream {
    static float m = 0;

    static int signum(int a, int b) {
        if( a == b )
            return 0;

        return a < b ? -1 : 1;
    }

    // Function implementing algorithm to find median so far.
    static float getMedian(int e, float m, PriorityQueue<Integer> l, PriorityQueue<Integer> r)
    {
        // Are heaps balanced? If yes, sig will be 0
        int sig = signum(l.size(), r.size());
        switch(sig)
        {
            case 1: // There are more elements in left (max) heap

                if( e < m ) // current element fits in left (max) heap
                {
                    // Remore top element from left heap and
                    // insert into right heap
                    r.add(l.poll());

                    // current element fits in left (max) heap
                    l.add(e);
                }
                else
                {
                    // current element fits in right (min) heap
                    r.add(e);
                }

                // Both heaps are balanced
                m = (l.peek() + r.peek()) / 2;

                break;

            case 0: // The left and right heaps contain same number of elements

                if( e < m ) // current element fits in left (max) heap
                {
                    l.add(e);
                    m = l.peek();
                }
                else
                {
                    // current element fits in right (min) heap
                    r.add(e);
                    m = r.peek();
                }

                break;

            case -1: // There are more elements in right (min) heap

                if( e < m ) // current element fits in left (max) heap
                {
                    l.add(e);
                }
                else
                {
                    // Remove top element from right heap and
                    // insert into left heap
                    l.add(r.poll());

                    // current element fits in right (min) heap
                    r.add(e);
                }

                // Both heaps are balanced
                m = (r.peek() + l.peek()) / 2;

                break;
        }

        // No need to return, m already updated
        return m;
    }


    static void printMedian(int A[], int size)
    {
         // effective median
        PriorityQueue<Integer> left = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> right = new PriorityQueue<>();

        for(int i = 0; i < size; i++)
        {
            m = getMedian(A[i], m, left, right);

            System.out.println(A[i] + ", " + m);
        }

    } // 1 2 3 5 6 7 8 9 10 15
    // Driver code
    public static void main(String[] args)
    {
        int A[] = {5, 15, 1, 3, 2, 8, 7, 9, 10, 6, 11, 4};
        int size = A.length;

        // In lieu of A, we can also use data read from a stream
        printMedian(A, size);
    }
}
