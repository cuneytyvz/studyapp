package com.cnyt.studyapp.study.array;

import java.util.Arrays;

public class LargestTwoProduct {

    static int[] find(int[] arr) {
        int max = 1, min = 1, maxMult = 1, minMult = 1,maxIndex = 1, minIndex = 1;
        int[] indexes = new int[2];

        for(int i = 0; i < arr.length; i++) {
            int n = arr[i];

            if(n * max > maxMult) {
                indexes[0] = maxIndex;
                indexes[1] = i;
                maxMult = n * max;
            }
            if(n * min > minMult) {
                indexes[0] = minIndex;
                indexes[1] = i;
                minMult = n * min;
            }
            if(max < n) {
                maxIndex = i;
                max = n;
            }
            if(min > n) {
                minIndex = i;
                min = n;
            }
        }

        return indexes;
    }

    public static void main(String[] args) {
        int[] result = find(new int[]{5,6,-10,4,-5});
        System.out.println(Arrays.toString(result));
    }
}
