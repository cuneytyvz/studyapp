package com.cnyt.studyapp.study.array;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MinimumSprinkle {
    static class Data {
        int index;
        int maxRange;

        Data(int index, int maxRange) {
            this.index = index;
            this.maxRange = maxRange;
        }
    }
    static int minTaps(int n, int[] ranges) {
        boolean[][] matrix = new boolean[n+1][n+1];
        for(int i=0; i<=n;i++) {
            for(int j=0; j<=n; j++) {
                if(Math.max(0,i - ranges[i]) <= j &&  i + ranges[i] >= j) {
                    matrix[i][j] = true;
                }
            }
        }

        Map<Integer,List<Data>> map = new TreeMap<>();
        for(int j=0; j<=n; j++) {
            for(int i=0; i<=n;i++) {
                if(matrix[i][j])
                    map.computeIfAbsent(j, k -> new ArrayList<>()).add(new Data(i,i+ranges[i]));
            }
        }

        int i = 0,count = 0;
        while(i <= n) {
            List<Data> list = map.get(i);
            if(list == null || list.isEmpty()) {
                count = -1;
                break;
            }
            int max = Integer.MIN_VALUE;
            Data newIndex = null;
            for(Data d : list) {
                if(d.maxRange > max) {
                    newIndex = d;
                    max = d.maxRange;
                }
            }

            if(newIndex == null)
                return -1;
            count++;
            i = newIndex.maxRange + 1;
        }

        return count;
    }

    public static void main(String[] args) {
        int result = minTaps(7,new int[]{3,4,1,1,0,1,0,1});
        System.out.println(result);
    }
}

// [3,4,1,1,0,1,0,1]
//    0   1   2   3   4   5   6   7
// 3  1   1   1   1   0   0
// 4  1   1   1   1   1   1
// 1  0   1   1   1   0   0
// 1  0   0   1   1   1   0
// 0  0   0   0   0   1   0
// 1  0   0   0   0   1   1   1
// 0                          1
// 1                          1   1

// 0  0->3,1->5
// 1  0->3,1->5,2->3
// 2  0->3,1->5,2->3,3->4
// 3  1->5,2->3,2->3,3->4
// 4  1->5,3->4,4->4,5->6
// 5  1->5,5->6
// 6  5->6,6->6,7->8
// 7  7->8