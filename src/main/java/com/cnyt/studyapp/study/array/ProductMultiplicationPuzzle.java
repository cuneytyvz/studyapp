package com.cnyt.studyapp.study.array;

import java.util.Arrays;

public class ProductMultiplicationPuzzle {

    static int[] find(int[] arr) {
        int[] straight = new int[arr.length], reversed = new int[arr.length];

        for(int i=0; i < arr.length; i++) {
            if(i==0)
                straight[i] = arr[i];
            else
                straight[i] = arr[i] * straight[i-1];
        }

        for(int i=arr.length-1; i >= 0; i--) {
            if(i==arr.length-1)
                reversed[i] = arr[i];
            else
                reversed[i] = arr[i] * reversed[i+1];
        }

        int[] result = new int[arr.length];
        for(int i=0; i < arr.length; i++) {
            if(i==0) {
                result[i]=reversed[i+1];
            } else if(i==arr.length-1) {
                result[i]=straight[i-1];
            } else {
                result[i] = reversed[i+1] * straight[i-1];
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] result = find(new int[]{2,2,3,4,5});
        System.out.println(Arrays.toString(result));
    }
}
