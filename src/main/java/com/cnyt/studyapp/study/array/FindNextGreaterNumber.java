package com.cnyt.studyapp.study.array;

import java.util.*;
import java.util.stream.Collectors;

public class FindNextGreaterNumber {

    static String swap(String n, char c1, char c2) {
        char temp1 = c1, temp2=c2;
        n=n.replace(c1, '-');
        n=n.replace(c2,temp1);
        n=n.replace('-',temp2);

        return n;
    }

    static String find(String n) {
        int k = -1;
        for (int i = n.length() - 1; i >= 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (n.charAt(i) > n.charAt(j)) {
                    k = j;

                    n=swap(n,n.charAt(i),n.charAt(j));
                    break;
                }
            }
            if (k != -1) break;
        }

        if(k == -1) return "Not Possible.";

        List<Integer> digitsToSort = Arrays.stream(n.substring(k + 1, n.length()).split("")).
                map(nn -> Integer.parseInt(nn)).sorted().collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        digitsToSort.stream().forEach(sb::append);

        return n.substring(0,k+1) + sb.toString();
    }

    static String findOptimized(String n) {
        Stack<Integer> stack = new Stack<>();

        stack.push(n.length()-1);
        int[] lesserIndexArr = new int[n.length()];
        Arrays.fill(lesserIndexArr,-1);
        boolean found = false;
        for(int i = n.length() - 2; i >= 0; i--){ // 534976
            while(!stack.isEmpty() && n.charAt(i) < n.charAt(stack.peek())) {
                lesserIndexArr[stack.peek()] = i;
                stack.pop();
                found = true;
            }
            if(found) break;
            stack.push(i);
        }
        if(!found) return "Not Possible";
        int i;
        for(i = n.length() - 1; i >= 0; i--){
            if(lesserIndexArr[i] != -1) {
                break;
            }
        }

        n=swap(n,n.charAt(i),n.charAt(lesserIndexArr[i]));

        List<Integer> digitsToSort = Arrays.stream(n.substring(lesserIndexArr[i] + 1, n.length()).split("")).
                map(nn -> Integer.parseInt(nn)).sorted().collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        digitsToSort.stream().forEach(sb::append);

        return n.substring(0,lesserIndexArr[i]+1) + sb.toString();
    }

//    Input: n = "539986"
//    Output: "536479"
//          534976 536974
//
//            3421
//            4123
//
//            4321
    public static void main(String[] args) {
//        System.out.println(find("534976"));
//        System.out.println(find("1234"));
//        System.out.println(find("4321"));

        System.out.println(findOptimized("7896"));
        System.out.println(findOptimized("123796"));
        System.out.println(findOptimized("1234"));
    }
}
