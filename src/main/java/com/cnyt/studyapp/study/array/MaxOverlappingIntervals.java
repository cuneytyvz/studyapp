package com.cnyt.studyapp.study.array;

import java.util.ArrayList;
import java.util.Collections;

public class MaxOverlappingIntervals {

    static class pair
    {
        int first;
        char second;

        pair(int first, char second)
        {
            this.first = first;
            this.second = second;
        }
    }

    // Function that print maximum
// overlap among ranges
    static void overlap(int[][] v)
    {

        // Variable to store the maximum
        // count
        int ans = 0;
        int count = 0;
        ArrayList<pair> data = new ArrayList<>();

        // Storing the x and y
        // coordinates in data vector
        for(int i = 0; i < v.length; i++)
        {

            // Pushing the x coordinate
            data.add(new pair(v[i][0], 'x'));

            // pushing the y coordinate
            data.add(new pair(v[i][1], 'y'));
        }

        // Sorting of ranges
        Collections.sort(data, (a, b) -> {
            if(a.first == b.first) {
                return a.second == 'x' ? -1 : 1;
            } else {
                return a.first - b.first;
            }
        });

        // Traverse the data vector to
        // count number of overlaps
        for(int i = 0; i < data.size(); i++)
        {
            // 1,x - 2,y - 2,x - 3,x - 4,y - 6,y
            // If x occur it means a new range
            // is added so we increase count
            if (data.get(i).second == 'x')
                count++;

            // If y occur it means a range
            // is ended so we decrease count
            if (data.get(i).second == 'y')
                count--;

            // Updating the value of ans
            // after every traversal
            ans = Math.max(ans, count);
        }

        // Printing the maximum value
        System.out.println(ans);
    }

    // Driver code
    public static void main(String[] args)
    {
//        int[][] v = { { 1, 2 },
//                { 2, 4 },
//                { 3, 6 } };

        int[][] v = {{1, 8}, {2, 5}, {5, 6}, {3, 7}};
        // 1,x - 2,x - 3,x - 5,x, - 5,y - 6,y - 7,y - 8,y
        // 1     2     3     4      3     2      1     0
        overlap(v);
    }

}
