package com.cnyt.studyapp.study.array;

public class MaximumPathSumMatrix {
    static int maxPath(int[][] m) {
        int[][] k = new int[m.length][m[0].length];
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < m.length; i++) {
            for(int j = 0; j < m.length; j++) {
                if(i == 0 && j == 0)
                    k[i][j] = m[i][j];
                else if(i == 0)
                    k[i][j] = k[i][j-1] + m[i][j];
                else if(j ==0)
                    k[i][j] = k[i-1][j] + m[i][j];
                else
                    k[i][j] = m[i][j] + Math.max(k[i-1][j],k[i][j-1]);

                max = Math.max(k[i][j],max);
            }
        }

        return max;
    }
    public static void main(String[] args) {
        int result = maxPath(new int[][] {
                {10, 30, 50},
                {0, 40, 30},
                {20, 70, 80}
        });

        System.out.println(result);
    }
}
