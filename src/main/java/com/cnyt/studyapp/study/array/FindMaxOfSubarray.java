package com.cnyt.studyapp.study.array;

import java.util.Stack;

public class FindMaxOfSubarray {

    // Function to print the maximum for
    // every k size sub-array
    static int counter=0;
    static void print_max(int a[], int n, int k)
    {
        // max_upto array stores the index
        // upto which the maximum element is a[i]
        // i.e. max(a[i], a[i + 1], ... a[max_upto[i]]) = a[i]

        int[] max_upto = new int[n];

        // Update max_upto array similar to
        // finding next greater element
        Stack<Integer> s = new Stack<>();
        s.push(0);
        for (int i = 1; i < n; i++)
        {
            while (!s.empty() && a[s.peek()] < a[i])
            {
                max_upto[s.peek()] = i - 1;
                s.pop();
                counter++;
            }
            s.push(i);
        }
        while (!s.empty())
        {
            max_upto[s.peek()] = n - 1;
            s.pop();
        }
        int j = 0;
        for (int i = 0; i <= n - k; i++)
        {

            // j < i is to check whether the
            // jth element is outside the window
            while (j < i || max_upto[j] < i + k - 1)
            {
                j++;
            }
            System.out.print(a[j] + " ");
        }
        System.out.println();
    }

    // 9 8 7 6 5 4 3 2 1
    // 9 8 7 6 5 4 3 2 1 stack
    // 8 8 8 8 8 8 8 8 8
    //  maxA, maxB
    // 15, 7, 2, 12, 6, 3, 2, 1, 5
    // stack     0
    // max_upto
    public static void main(String[] args) {
//        int[] arr = new int[]{9, 7, 2, 4, 6, 8, 2, 1, 5};
        int[] arr = new int[]{9,8,7,6,1,2,3,4,5};
        print_max(arr, arr.length,3);
        System.out.println("Counter = " + counter);
    }
}
