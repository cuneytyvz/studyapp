package com.cnyt.studyapp.study.array;

public class RottenTomatoes {
    // -1,0 +1,0  0,-1, 0,+1
    static int[] is = new int[]{-1,0,0,1};
    static int[] js = new int[]{0,-1,1,0};
    static boolean isSafe(int[][] grid,int i,int ii, int j, int jj) {
        return i+ii >= 0 && i+ii < grid.length && j+jj >= 0 && j+jj < grid[0].length
                && grid[i+ii][j+jj] >= 2;
    }

    static int orangesRotting(int[][] grid) {
        int max = 0;
        for(int i=0; i<grid.length; i++){
            for(int j=0;j<grid[0].length;j++) {
                if(grid[i][j] == 1) {
                    int min = Integer.MAX_VALUE;
                    for(int k=0;k<4;k++){
                        if(isSafe(grid,i,is[k],j,js[k])) {
                            min=Math.min(grid[i+is[k]][j+js[k]],min);
                        }
                    }
                    if(min == Integer.MAX_VALUE)
                        return -1;
                    grid[i][j] = min+1;
                    max = Math.max(max,min+1);
                }
            }
        }

        return Math.max(0,max-2);
    }

    // YANLIS
    public static void main(String[] args) {
//        int result = orangesRotting(new int[][]{
//                {1,2,1},
//                {1,1,0},
//                {0,1,1}});
//        int result = orangesRotting(new int[][]{
//                {2,1,1},
//                {0,1,1},
//                {1,0,1}});
        int result = orangesRotting(new int[][]{
                {2,0,1,1,1,1,1,1,1,1},
                {1,0,1,0,0,0,0,0,0,1},
                {1,0,1,0,1,1,1,1,0,1},
                {1,0,1,0,1,0,0,1,0,1},
                {1,0,1,0,1,0,0,1,0,1},
                {1,0,1,0,1,1,0,1,0,1},
                {1,0,1,0,0,0,0,1,0,1},
                {1,0,1,1,1,1,1,1,0,1},
                {1,0,0,0,0,0,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,1}});
        System.out.println(result);
    }
}
