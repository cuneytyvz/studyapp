package com.cnyt.studyapp.study.array;

public class TraverseLand {
    static final int LAND = 1;
    static final int SEA = 0;
    static final int OBSTACLE = 9;

    static int findMin(int[][] m){
        int count = 0;
        for(int i = 0; i < m.length; i++) {
            for(int j = 0; j < m[0].length; j++) {
                if(i == 0 && j == 0) {
                    m[i][j] = 1;
                } else if (m[i][j] != 0) {
                    if (i != m.length - 1 && j != m[0].length - 1) {
                        m[i][j] = Math.min(Math.min(m[i - 1][j], m[i][j - 1]), Math.min(m[i + 1][j], m[i][j + 1])) + 1;
                    } else if(i == 0 && j != m[0].length - 1) {
                        m[i][j] = Math.min(m[i][j - 1], Math.min(m[i + 1][j], m[i][j + 1])) + 1;
                    } else if(i != m.length - 1 && j == 0) {
                        m[i][j] = Math.min(m[i - 1][j], Math.min(m[i + 1][j], m[i][j + 1])) + 1;
                    } else if(i == m.length - 1 && j == m[0].length - 1) {
                        m[i][j] = Math.min(m[i - 1][j], m[i][j - 1]) + 1;
                    }
                }

            }
        }

        return 0;
    }

    public static void main(String[] args) {
        System.out.println(findMin(new int[][]{
                {1, 1, 0, 0},
                {0, 1, 1, 0},
                {0, 0, 1, 0},
                {0, 1, 1, 0},
                {0, 9, 0, 0}
        }));
    }
}
