package com.cnyt.studyapp.study.array;

import java.util.Collections;
import java.util.Vector;

public class MinimumSprinklersOriginal {
    static class Pair {
        int first;
        int second;

        Pair(int f, int s) {
            first = f;
            second = s;
        }
    }
    // Function to find minimum number of
// sprinkler to be turned on
    static int minSprinklers(int arr[], int N)
    {
        // Stores the leftmost and rightmost
        // point of every sprinklers
        Vector<Pair> V = new Vector<>();
        // Traverse the array arr[]
        for (int i = 0; i < N; i++) {
            if (arr[i] > -1) {
                V.add(new Pair(i - arr[i], i + arr[i]));
            }
        }
        // Sort the array sprinklers in
        // ascending order by first element
        Collections.sort(V,(p1,p2)-> p1.first - p2.first);

        // Stores the rightmost range
        // of a sprinkler
        int maxRight = 0;
        // Stores minimum sprinklers
        // to be turned on
        int res = 0;

        int i = 0;

        // -1,3 - -1,5 - 4,4 - 5,5 - 7,7
        // Iterate until maxRight is
        // less than N
        while (maxRight < N) {

            // If i is equal to V.size()
            // or V[i].first is greater
            // than maxRight

            if (i == V.size() || V.get(i).first > maxRight) {
                return -1;
            }
            // Stores the rightmost boundary
            // of current sprinkler
            int currMax = V.get(i).second;

            // Iterate until i+1 is less
            // than V.size() and V[i+1].first
            // is less than or equal to maxRight
            while (i + 1 < V.size()
                    && V.get(i+1).first <= maxRight) {

                // Increment i by 1
                i++;
                // Update currMax
                currMax = Math.max(currMax, V.get(i).second);
            }

            // If currMax is less than the maxRight
            if (currMax < maxRight) {
                // Return -1
                return -1;
            }
            // Increment res by 1
            res++;

            // Update maxRight
            maxRight = currMax + 1;

            // Increment i by 1
            i++;
        }
        // Return res as answer
        return res;
    }

    // Drive code.
    public static void main(String[] args)
    {
        // Input
        int arr[] = { -1, 2, 3, -1, 0, 0,-1,0 };
        int N = arr.length;

        // Function call
        int result = minSprinklers(arr, N);
        System.out.println(result);
    }
}
