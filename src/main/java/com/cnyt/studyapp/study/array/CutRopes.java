package com.cnyt.studyapp.study.array;

import java.util.Arrays;
import java.util.Collections;

public class CutRopes {
    // Function to find the maximum size
    // of ropes having frequency at least
    // K by cutting the given ropes
    static int maximumSize(Integer[] a,
                           int k)
    {
        // Stores the left and
        // the right boundaries
        int low = 1;
        int high = Collections.max(
                Arrays.asList(a));

        // Stores the maximum length
        // of rope possible
        int ans = -1;

        // Iterate while low is less
        // than or equal to high
        while (low <= high) {

            // Stores the mid value of
            // the range [low, high]
            int mid = low + (high - low) / 2;

            // Stores the count of ropes
            // of length mid
            int count = 0;

            // Traverse the array arr[]
            for (int c = 0;
                 c < a.length; c++) {
                count += a[c] / mid;
            }

            // If count is at least K
            if (count >= k) {

                // Assign mid to ans
                ans = mid;

                // Update the value
                // of low
                low = mid + 1;
            }

            // Otherwise, update the
            // value of high
            else {
                high = mid - 1;
            }
        }

        // Return the value of ans
        return ans;
    }

    // Driver Code
    public static void main(String[] args)
    {
        Integer[] arr = { 5, 2, 7, 4, 9};
        int K = 5;
        System.out.println(
                maximumSize(arr, K));
    }
}
