package com.cnyt.studyapp.study.array;

import java.util.PriorityQueue;

public class ConnectRopes {
    static int connect(int[] arr) {
        PriorityQueue<Integer> heap = new PriorityQueue<>();

        int totalCost = 0;
        for(int n : arr)
            heap.add(n);

        int min1, min2;
        while(heap.size() > 1){
            min1 = heap.poll();
            min2 = heap.poll();
            int cost = min1+min2;
            heap.add(cost);
            totalCost+=cost;
        }

        return totalCost;
    }

    public static void main(String[] args) {
        System.out.println(connect(new int[]{
            8, 5, 1,2
        }));
    }
}
