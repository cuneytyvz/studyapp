package com.cnyt.studyapp.study.array;

public class BuySellStock {
    public static int maxProfit(int[] prices) {
        int maxProfit = Integer.MIN_VALUE;
        int minValue = Integer.MAX_VALUE;
        for(int price : prices) {
            minValue = Math.min(price,minValue);
            maxProfit = Math.max(maxProfit,price - minValue);
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,2,5,3,6,4,1,10}));
    }
}
