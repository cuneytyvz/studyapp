package com.cnyt.studyapp.study.array;

public class FindElementInRotatedArray {
    static int find(int[] arr, int n) {
        return helper(arr,0,arr.length-1,n);
    }

    static int count1 = 0, count2 = 0;
    static int helper(int[] arr, int left, int right, int n) {
        count1++;
        if(left > right) return -1;
        int mid = (right + left) / 2;
        if(left == right && arr[mid] != n) return -1;
        if(arr[mid] == n) return mid;

        int leftSide = -1, rightSide = -1;
        if(arr[mid] > n) leftSide = helper(arr,left,mid-1,n);
        if(leftSide != -1) return leftSide;

        rightSide = helper(arr,mid+1,right,n);
        if(rightSide != -1) return rightSide;

        return -1;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 2, 3};
//        int[] arr = new int[]{3,4,5,1,2};
        int index = find(arr, 1);
        System.out.println(index + ", count1 = " + count1);
        index = pivotedBinarySearch(arr,arr.length, 1);
        System.out.println(index + ", count2 = " + count2);
    }

    /* Searches an element key in a
       pivoted sorted array arrp[]
       of size n */
    static int pivotedBinarySearch(int arr[], int n, int key)
    {
        int pivot = findPivot(arr, 0, n - 1);

        // If we didn't find a pivot, then
        // array is not rotated at all
        if (pivot == -1)
            return binarySearch(arr, 0, n - 1, key);

        // If we found a pivot, then first
        // compare with pivot and then
        // search in two subarrays around pivot
        if (arr[pivot] == key)
            return pivot;
        if (arr[0] <= key)
            return binarySearch(arr, 0, pivot - 1, key);
        return binarySearch(arr, pivot + 1, n - 1, key);
    }

    /* Function to get pivot. For array
       3, 4, 5, 6, 1, 2 it returns
       3 (index of 6)
       5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 2, 3 */
    static int findPivot(int arr[], int low, int high)
    {
        count2++;
        // base cases
        if (high < low)
            return -1;
        if (high == low)
            return low;

        /* low + (high - low)/2; */
        int mid = (low + high) / 2;
        if (mid < high && arr[mid] > arr[mid + 1])
            return mid;
        if (mid > low && arr[mid] < arr[mid - 1])
            return (mid - 1);
        if (arr[low] >= arr[mid])
            return findPivot(arr, low, mid - 1);
        return findPivot(arr, mid + 1, high);
    }

    /* Standard Binary Search function */
    static int binarySearch(int arr[], int low, int high, int key)
    {
        count2++;
        if (high < low)
            return -1;

        /* low + (high - low)/2; */
        int mid = (low + high) / 2;
        if (key == arr[mid])
            return mid;
        if (key > arr[mid])
            return binarySearch(arr, (mid + 1), high, key);
        return binarySearch(arr, low, (mid - 1), key);
    }

}
