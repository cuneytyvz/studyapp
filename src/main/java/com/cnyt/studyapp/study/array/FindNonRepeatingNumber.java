package com.cnyt.studyapp.study.array;

public class FindNonRepeatingNumber {
    static int find(int[] arr) {
        return search(arr,0,arr.length-1);
    }

    static int search(int[] arr, int left, int right) {
        if(left > right) return -1;
        int mid = (left + right) / 2;

        if(mid == 1 && arr[mid - 1] != arr[mid])
            return arr[mid-1];

        if(mid == arr.length-2 && arr[mid] != arr[mid+1])
            return arr[mid+1];

        if(arr[mid-1] != arr[mid] && arr[mid+1] != arr[mid])
            return arr[mid];

        if(arr[mid-1] == arr[mid])
            return search(arr,left,mid);

        if(arr[mid+1] == arr[mid])
            return search(arr,mid,right);

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(find(new int[]{1,1,2,2,3,3,4,5,5}));
    }
}
