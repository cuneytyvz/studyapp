package com.cnyt.studyapp.openpayd;

import com.cnyt.studyapp.openpayd.dto.TransactionDto;
import com.cnyt.studyapp.openpayd.model.Transaction;
import com.cnyt.studyapp.openpayd.repository.TransactionRepository;
import com.cnyt.studyapp.openpayd.service.external.RatesApiService;
import com.cnyt.studyapp.openpayd.service.impl.ExchangeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@SpringBootTest
public class ExchangeServiceTest {
    @MockBean
    private RatesApiService ratesApiService;
    @MockBean
    private TransactionRepository transactionRepository;
    @Autowired
    private ExchangeServiceImpl exchangeService;

    @BeforeEach
    void before() {
        Mockito.when(ratesApiService.getExchangeRate("USD", "TRY")).thenReturn(8.0);
        Mockito.when(transactionRepository.save(Mockito.any())).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(transactionRepository.findById(1l)).thenReturn(
                Optional.of(new Transaction(1l, new Date(), new BigDecimal(10), "USD", new BigDecimal(80), "TRY")));
        Mockito.when(transactionRepository.findAllByDateBetween(Mockito.any(),Mockito.any(), Mockito.eq(PageRequest.of(0,5)))).thenReturn(
                Arrays.asList(
                        new Transaction(1l, new Date(), new BigDecimal(10), "USD", new BigDecimal(80), "TRY"),
                        new Transaction(2l, new Date(), new BigDecimal(10), "GBP", new BigDecimal(100), "TRY")
                ));
    }

    @Test
    void testGetExchangeRate() {
        Double exchangeRate = exchangeService.getExchangeRate("USD","TRY");

        Assertions.assertEquals(8.0,exchangeRate);
    }

    @Test
    void testGetConversion() {
        TransactionDto transactionDto = exchangeService.getConversion(new BigDecimal(10),"USD","TRY");

        Assertions.assertEquals(new BigDecimal(80).setScale(4, RoundingMode.HALF_DOWN), transactionDto.getTargetAmount());
    }

    @Test
    void testGetConversionListById() {
        List<TransactionDto> transactions = exchangeService.getConversionList(1l, null, 0,  0);

        Assertions.assertEquals(transactions.get(0).getId(), 1l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Assertions.assertEquals(transactions.get(0).getDate(), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(0).getSourceAmount(), new BigDecimal(10));
        Assertions.assertEquals(transactions.get(0).getTargetAmount(), new BigDecimal(80));
        Assertions.assertEquals(transactions.get(0).getSourceCurrency(), "USD");
        Assertions.assertEquals(transactions.get(0).getTargetCurrency(), "TRY");
    }

    @Test
    void testGetConversionListByDate() {
        List<TransactionDto> transactions = exchangeService.getConversionList(null, new Date(), 0,  5);

        Assertions.assertEquals(2,transactions.size());
        Assertions.assertEquals(transactions.get(0).getId(), 1l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Assertions.assertEquals(transactions.get(0).getDate(), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(0).getSourceAmount(), new BigDecimal(10));
        Assertions.assertEquals(transactions.get(0).getTargetAmount(), new BigDecimal(80));
        Assertions.assertEquals(transactions.get(0).getSourceCurrency(), "USD");
        Assertions.assertEquals(transactions.get(0).getTargetCurrency(), "TRY");

        Assertions.assertEquals(transactions.get(1).getId(), 2l);
        Assertions.assertEquals(transactions.get(1).getDate(), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(1).getSourceAmount(), new BigDecimal(10));
        Assertions.assertEquals(transactions.get(1).getTargetAmount(), new BigDecimal(100));
        Assertions.assertEquals(transactions.get(1).getSourceCurrency(), "GBP");
        Assertions.assertEquals(transactions.get(1).getTargetCurrency(), "TRY");

    }
}
