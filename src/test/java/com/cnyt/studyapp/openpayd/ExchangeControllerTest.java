package com.cnyt.studyapp.openpayd;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.cnyt.studyapp.openpayd.dto.TransactionDto;
import com.cnyt.studyapp.openpayd.service.ExchangeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class ExchangeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExchangeService exchangeService;

    @BeforeEach
    public void setup(){
        Mockito.when(exchangeService.getExchangeRate("USD","TRY")).thenReturn(8.0);
        Mockito.when(exchangeService.getConversion(new BigDecimal(10),"USD","TRY")).thenReturn(
                new TransactionDto(1l,"2021-04-01 00:00",new BigDecimal(10),"USD",new BigDecimal(80),"TRY")
        );
        Mockito.when(exchangeService.getConversionList(1l, null, null, null)).thenReturn(
                Arrays.asList(new TransactionDto(1l,new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()),new BigDecimal(10),"USD",new BigDecimal(80),"TRY"))
        );
        Mockito.when(exchangeService.getConversionList(Mockito.any(), Mockito.any(), Mockito.eq(0), Mockito.eq(5))).thenReturn(
                Arrays.asList(
                        new TransactionDto(1l,new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()),new BigDecimal(10),"USD",new BigDecimal(80),"TRY"),
                        new TransactionDto(1l,new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()),new BigDecimal(10),"GBP",new BigDecimal(100),"TRY")
                )
        );
    }

    @Test
    void testExchangeRate() throws  Exception {
        String responseStr = mvc.perform(get("/exchangeRate?source=USD&target=TRY").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Map response = new ObjectMapper().readValue(responseStr, Map.class);

        Assertions.assertEquals(200,response.get("code"));
        Assertions.assertEquals(8.0,response.get("data"));
    }

    @Test
    void testConversion() throws  Exception {
        String responseStr = mvc.perform(get("/conversion?amount=10&source=USD&target=TRY").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();
        Map response = new ObjectMapper().readValue(responseStr, Map.class);

        Assertions.assertEquals(201,response.get("code"));
        TransactionDto transactionDto = new ObjectMapper().convertValue(response.get("data"), TransactionDto.class);
        Assertions.assertEquals(new BigDecimal(80),transactionDto.getTargetAmount());
    }

    @Test
    void testConversionListById() throws  Exception {
        String responseStr = mvc.perform(get("/conversions?id=1").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Map response = new ObjectMapper().readValue(responseStr, Map.class);

        Assertions.assertEquals(200,response.get("code"));
        List<LinkedHashMap> transactions = new ObjectMapper().convertValue(response.get("data"), List.class);

        Assertions.assertEquals(transactions.get(0).get("id"), 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Assertions.assertEquals(transactions.get(0).get("date"), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(0).get("sourceAmount"),10);
        Assertions.assertEquals(transactions.get(0).get("targetAmount"), 80);
        Assertions.assertEquals(transactions.get(0).get("sourceCurrency"), "USD");
        Assertions.assertEquals(transactions.get(0).get("targetCurrency"), "TRY");
    }

    @Test
    void testConversionListByDate() throws  Exception {
        String responseStr = mvc.perform(get("/conversions?date=2021-04-01&page=1&pageSize=5").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Map response = new ObjectMapper().readValue(responseStr, Map.class);

        Assertions.assertEquals(200,response.get("code"));
        List<LinkedHashMap> transactions = new ObjectMapper().convertValue(response.get("data"), List.class);

        Assertions.assertEquals(2,transactions.size());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Assertions.assertEquals(transactions.get(0).get("date"), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(0).get("sourceAmount"),10);
        Assertions.assertEquals(transactions.get(0).get("targetAmount"), 80);
        Assertions.assertEquals(transactions.get(0).get("sourceCurrency"), "USD");
        Assertions.assertEquals(transactions.get(0).get("targetCurrency"), "TRY");

        Assertions.assertEquals(transactions.get(1).get("date"), sdf.format(new Date()));
        Assertions.assertEquals(transactions.get(1).get("sourceAmount"),10);
        Assertions.assertEquals(transactions.get(1).get("targetAmount"), 100);
        Assertions.assertEquals(transactions.get(1).get("sourceCurrency"), "GBP");
        Assertions.assertEquals(transactions.get(1).get("targetCurrency"), "TRY");
    }
}
