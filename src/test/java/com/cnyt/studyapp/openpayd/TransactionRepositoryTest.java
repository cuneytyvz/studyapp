package com.cnyt.studyapp.openpayd;

import com.cnyt.studyapp.openpayd.model.Transaction;
import com.cnyt.studyapp.openpayd.repository.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase
public class TransactionRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    void testFindById() {
        Transaction transaction = new Transaction(null, new Date(), new BigDecimal(10), "USD", new BigDecimal(80), "TRY");
        entityManager.persist(transaction);
        entityManager.flush();

        Transaction found = transactionRepository.findById(transaction.getId()).get();

        Assertions.assertEquals("USD",found.getSourceCurrency());
        Assertions.assertEquals("TRY",found.getTargetCurrency());
    }

    @Test
    void testFindByDate() {
        Transaction transaction = new Transaction(null, new Date(), new BigDecimal(10), "USD", new BigDecimal(80), "TRY");
        entityManager.persist(transaction);
        transaction = new Transaction(null, new Date(), new BigDecimal(10), "GBP", new BigDecimal(100), "TRY");
        entityManager.persist(transaction);
        entityManager.flush();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY,00);
        cal.set(Calendar.MINUTE,00);
        cal.set(Calendar.SECOND,00);
        Date start = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,59);
        Date end = cal.getTime();
        List<Transaction> found = transactionRepository.findAllByDateBetween(start,end, PageRequest.of(0,5));

        Assertions.assertEquals(2,found.size());
        Assertions.assertEquals("USD",found.get(0).getSourceCurrency());
        Assertions.assertEquals("GBP",found.get(1).getSourceCurrency());
    }
}